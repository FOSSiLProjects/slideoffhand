<?php

date_default_timezone_set("America/Phoenix");

// Set up variables for the event's title, date, branch, and the colour of the bar.

$title = $_POST['title'];
$date = $_POST['date'];
$branch = $_POST['branch'];
$bar = $_POST['bar'];
$tstamp = date("Ymd-His");

if($branch == 'Amuro Branch Library'):
	$bname = 'amu';
	elseif ($branch == 'Baez Branch Library'):
	$bname = 'bae';
	elseif ($branch == 'Baker Regional Library'):
	$bname = 'bak';
	elseif ($branch == 'Bourdin Branch Library'):
	$bname = 'bou';
	elseif ($branch == 'Bowie Branch Library'):
	$bname = 'bow';
	elseif ($branch == 'Clancy Branch Library'):
	$bname = 'cla';
	elseif ($branch == 'Collins Branch Library'):
	$bname = 'col';
	elseif ($branch == 'Crothers Branch Library'):
	$bname = 'cro';
	elseif ($branch == 'Evangelos Branch Library'):
	$bname = 'eva';
	elseif ($branch == 'Germanotta Regional Library'):
	$bname = 'ger';
	elseif ($branch == 'Harry Branch Library'):
	$bname = 'har';
	elseif ($branch == 'Hawkins Branch Library'):
	$bname = 'haw';
	elseif ($branch == 'Kelly Branch Library'):
	$bname = 'kel';
	elseif ($branch == 'Monae Branch Library'):
	$bname = 'mon';
	elseif ($branch == 'Rollins Branch Library'):
	$bname = 'rol';
	elseif ($branch == 'Smith Branch Library'):
	$bname = 'smi';
	elseif ($branch == 'Sudo Branch Library'):
  $bname = 'sud';
	else:
	$bname = 'wag';
endif;

$slidename = "slide-{$bname}-{$tstamp}.png";


// File upload

if(isset($_FILES['image'])){
		$errors= array();
		$file_name = $_FILES['image']['name'];
		$file_size =$_FILES['image']['size'];
		$file_tmp =$_FILES['image']['tmp_name'];
		$file_type=$_FILES['image']['type'];
		$file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));

		$expensions= array("jpeg","jpg","png");
		if(in_array($file_ext,$expensions)=== false){
			$errors[]="extension not allowed, please choose a JPEG or PNG file.";
		}
		if($file_size > 2097152){
		$errors[]='File size must be less than 2 MB';
		}
		if(empty($errors)==true){
			move_uploaded_file($file_tmp,"images/".$file_name);
			echo "Slide created:";
		}else{
			print_r($errors);
		}
	}

// Create slide using ImageMagick

exec("convert -background none -fill white -font \"LubalinGraItcTee\" -size 318x70 -gravity West caption:\"".$title."\" eventtitle.png");

exec("convert -background none -fill white -font \"NewsGoth-BT-Roman\" -size 300x23 -gravity East caption:\"".$date."\" eventdate.png");

exec("convert -background none -fill white -font \"NewsGoth-BT-Roman\" -size 289x23 -gravity East caption:\"".$branch."\" eventbranch.png");

exec("convert -size 740x250 xc:#a4343a -page +0+0 headercrop.jpg -page +0+175 $bar.png -page +632+175 logo.png -page +15+176 eventtitle.png -page +320+191 eventdate.png -page +330+212 eventbranch.png -layers flatten $slidename");

unlink('resized.jpg');

echo $slidename;

?>

<br>
<img src="<?=$slidename?>">
