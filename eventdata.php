<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MCLD Slide Generator - Event Data</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/simple-sidebar.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">

<form action="createslide.php" target="iframe" method="post" enctype="multipart/form-data">
<div>

<h2>Web Slide Code Generator</h2>

<label>
	<span>Event title:</span> <input type="text" name="title"><br><br>
</label>

<label>
	<span>Event date:</span> <input type="text" name="date"><br><br>
</label>

<label>
	<span>Branch:</span>
	<select name="branch">
	<option value="Amuro Branch Library">Amuro</option>
	<option value="Baez Branch Library">Baez</option>
	<option value="Baker Regional Library">Baker</option>
	<option value="Bourdin Branch Library">Bourdin</option>
	<option value="Bowie Branch Library">Bowie</option>
	<option value="Clancy Branch Library">Clancy</option>
	<option value="Collins Branch Library">Collins</option>
	<option value="Crothers Branch Library">Crothers</option>
	<option value="Evangelos Branch Library">Evangelos</option>
	<option value="Germanotta Regional Library">Germanotta</option>
	<option value="Harry Regional Library">Harry</option>
	<option value="Hawkins Branch Library">Hawkins</option>
	<option value="Kelly Branch Library">Kelly</option>
	<option value="Monae Branch Library">Monae</option>
	<option value="Rollins Branch Library">Rollins</option>
	<option value="Smith Branch Library">Smith</option>
	<option value="Sudo Branch Library">Sudo</option>
  <option value="Wagner Regional Library">Wagner</option>
	</select>
</label>

<label>
	<span>Bar colour:</span>
	<select name="bar">
	<option value="bluebar">Blue</option>
	<option value="greybar">Grey</option>
	<option value="purplebar">Purple</option>
	<option value="redbar">Red</option>
	</select>
</label>

<script>
function setURL(url){
	document.getElementById('iframe').src = url;
}
</script>
<button type="submit" >Generate Slide</button>
<br><br>
<p style="font-size: 10px;"><a href="index.php">Create a new slide.</a></p>
</div>
</form>

        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						<div id="bname"><h1>Your slide will generate here.</h1></div>
                        <iframe name="iframe" width="800" height="400" frameborder="no" border="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</div>


</body>

</html>
