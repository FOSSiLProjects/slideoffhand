# SlideOffHand

## Automating web slide creation

Libraries often promote events on their websites using a carousel of slides. These slides usually combine images, text, and perhaps a logo in a standard layout. Often, the work is done in an image editor such as Photoshop, Pixelmator, or GIMP as the slide-maker edits and moves around various bits of a template. When faced with dozens, maybe even hundreds, of slides per month; you'll find you can save a lot of time if you add a bit of automation to the process.

SlideOffHand is a browser based app that automates the slide creation process. By using a standard layout, you can create unique, but consistent slides. SlideOffHand presents you with a way to upload, and then crop, an image for your slide. Then enter your event information, choose a branch from a dropdown (preventing typos), and select a colour for the text area. Your library's logo can be added to the slide as well, marking it for use on other sites if you desire.

SlideOffHand requires:

- A server running a LAMP stack (Linux, Apache, MariaDB, and PHP)
  - *Note: Database functionality will be added in future releases*
- [ImageMagick](https://www.imagemagick.org/script/index.php)

You can make changes to the slide layouts in the code. Future releases will allow greater flexibility on the user-side without needing to muck about in the code.

### Interested? Need help?

If you want to have a play and need help setting it up, I'm here for you. Contact me at cyberpunklibrarian [at] protonmail [dot] com. You can also find me on Twitter as [@bibrarian](https://twitter.com/bibrarian) or on the glammr.us Mastodon instance as [@cybcerpunklibrarian@glammr.us](https://glammr.us/@cyberpunklibrarian).

## Example Images

**Blue Slide Example**

![Blue slide example](http://i.imgur.com/BUSMIB4.png)

**Grey Slide Example**

![Grey Slide Example](http://i.imgur.com/YvmrrHb.png)

**Purple Slide Example**

![Purple Slide Example](http://i.imgur.com/qygyiao.png)

**Red Slide Example**

![Red Slide Example](http://i.imgur.com/Mvdx6w4.png)

Fonts can be changed within ImageMagick calls and informational bar colours can be altered with Photoshop or GIMP.

## Current workflow

**Step 1. index.php**

First thing accessed by the user. Prompts for upload of the image file. Image file should be at least 780 x 175, but this is not enforced by the code at this time.

------

**Step 2. imgprep.php**

The user will not see this as it happens in the background. The uploaded image is resized to a width of 780, while keeping the aspect ratio intact. This not only makes cropping easier, but it also allows more of the image to be cropped in the next step.

------

**Step 3. crop.php**

The newly resized image is presented to the user with a simple cropping interface. The initial size of the cropping box is the exact size of the image header on the slide. Once the crop is selected...

------

**Step 4. eventdata.php**

This is the data entry screen and is the final screen seen by the user. A simple form allows for entry of the event's title, date and time, branch, and selection of a bar colour. Once complete, the next step is run, and displayed on the right side of the split screen.

------

**Step 5. createslide.php**

The final step and assembly of the elements. This exectutes behind the scenes and won't be seen by the user. The cropped header from step 3 is combined with the event data into a final slide. This slide is presented on the right side of the eventdata.php screen.

### Credits

SlideOffHand contains code from:

- Start Bootstrap - [Simple Sidebar.](http://startbootstrap.com/template-overviews/simple-sidebar/) Released under an MIT License.
- Deep Liquid - [Jcrop.](http://deepliquid.com/content/Jcrop.html) Released under an MIT License.
- StackOverflow - [PHP Questions.](http://stackoverflow.com/questions/tagged/php) With gratitude.

Coded in PHP. Utilizing ImageMagick.

- [PHP](http://php.net)
- [ImageMagick](http://www.imagemagick.org)